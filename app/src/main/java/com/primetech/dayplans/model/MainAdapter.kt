package com.primetech.dayplans.model

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.primetech.dayplans.R
import com.primetech.dayplans.utils.`object`.Main




class MainAdapter(val listItem: ArrayList<Main>) : RecyclerView.Adapter<MainAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_recycler, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val main = listItem[position]
        val category : String? = main.category
        if (category == "map"){
            holder.imgvPhoto.layoutParams.width = 350
        }
        else{
            holder.imgvPhoto.layoutParams.width = 150
        }
        Glide.with(holder.itemView.context)
            .load(main.photo)
            .into(holder.imgvPhoto)

    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgvPhoto: ImageView = itemView.findViewById(R.id.imgv_item)
    }
}