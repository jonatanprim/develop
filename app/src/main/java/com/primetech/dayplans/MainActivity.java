package com.primetech.dayplans;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.primetech.dayplans.model.MainAdapter;
import com.primetech.dayplans.model.ParisAdapter;
import com.primetech.dayplans.utils.object.DataMain;
import com.primetech.dayplans.utils.object.DataParis;
import com.primetech.dayplans.utils.object.Main;
import com.primetech.dayplans.utils.object.Paris;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //Variable Initialization
    private RecyclerView rvParis;
    private ArrayList<Paris> list = new ArrayList<>();
    private RecyclerView rvMain;
    private ArrayList<Main> list2 = new ArrayList<>();
    ImageView imgv_updown;
    LinearLayout detail_sight;
    ScrollView scroll;
    int click;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //RecyclerView Initialization
        rvParis = findViewById(R.id.recyc_paris);
        rvParis.setHasFixedSize(true);
        rvMain = findViewById(R.id.recyc_main);
        rvMain.setHasFixedSize(true);

        //Widget and click value Initialization
        imgv_updown = findViewById(R.id.imgv_detail_sight);
        detail_sight= findViewById(R.id.layout_detail);
        scroll = findViewById(R.id.scroll);
        click = 1;
        check();

        //Add data from Class Data
        list.addAll(DataParis.getListData());
        showParisRecyclerList();
        list2.addAll(DataMain.getListData());
        showMainRecyclerList();



        //event imgv_updown when onclick
        imgv_updown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click++;
                check();
            }
        });

    }


    //check condition of  imv_updown
    private void check(){
        if(click % 2 != 0){
            detail_sight.setVisibility(View.GONE);
            imgv_updown.setImageResource(R.drawable.ic_down);
            scroll.isSmoothScrollingEnabled();
        }
        else{
            detail_sight.setVisibility(View.VISIBLE);
            imgv_updown.setImageResource(R.drawable.ic_up);

            //Auto scroll when detail_sight VISIBLE
            scroll.post(new Runnable() {
                @Override
                public void run() {
                    scroll.smoothScrollTo(0,650);
                    scroll.isSmoothScrollingEnabled();
                }
            });
        }
    }

    private void showParisRecyclerList(){
        //Set RecyclerView Horizontal
        rvParis.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        //set Adapter
        ParisAdapter parisAdapter = new ParisAdapter(list);
        rvParis.setAdapter(parisAdapter);
    }

    private void showMainRecyclerList(){
        //Set RecyclerView Horizontal
        rvMain.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL,false));
        //set Adapter
        MainAdapter mainAdapter = new MainAdapter(list2);
        rvMain.setAdapter(mainAdapter);
    }
}
