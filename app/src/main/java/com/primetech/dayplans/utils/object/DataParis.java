package com.primetech.dayplans.utils.object;

import com.primetech.dayplans.R;

import java.util.ArrayList;

public class DataParis {
    private static String[] category = {
            "map",
            "view",
            "view",
            "view",
            "view",
            "view"
    };

    private static int[] photo = {
            R.drawable.parismap,
            R.drawable.paris1,
            R.drawable.paris2,
            R.drawable.paris3,
            R.drawable.paris4,
            R.drawable.paris5
    };


    public static ArrayList<Paris> getListData() {
        ArrayList<Paris> list = new ArrayList<>();
        for (int position = 0; position < category.length; position++) {
            Paris paris = new Paris();
            paris.setCategory(category[position]);
            paris.setPhoto(photo[position]);
            list.add(paris);
        }
        return list;
    }
}
