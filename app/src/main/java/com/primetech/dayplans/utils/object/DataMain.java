package com.primetech.dayplans.utils.object;

import com.primetech.dayplans.R;

import java.util.ArrayList;

public class DataMain {
    private static String[] category = {
            "map",
            "view",
            "view",
            "view"
    };

    private static int[] photo = {
            R.drawable.mainmap,
            R.drawable.main1,
            R.drawable.main2,
            R.drawable.main3
    };


    public static ArrayList<Main> getListData() {
        ArrayList<Main> list = new ArrayList<>();
        for (int position = 0; position < category.length; position++) {
            Main main = new Main();
            main.setCategory(category[position]);
            main.setPhoto(photo[position]);
            list.add(main);
        }
        return list;
    }
}
